# 1、基本

查看 https://gitee.com/ok-cloud/ok-eureka 的功能清单，后续新项目以此为标准！！！

# 2、其他

- [ ] /actuator/info端点信息
- [x] swagger
- [x] 全局异常
- [x] security端点接口拦截
- [ ] 打印请求&响应日志信息
- [ ] apollo 动态路由配置
- [ ] Hystrix断路器
- [ ] 限流
- [ ] 报文转换器
- [ ] 条件控制订阅者和订阅者凭证

https://zhuanlan.zhihu.com/p/382247014


