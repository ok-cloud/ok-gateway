package cn.xlbweb.gateway.filter;

import cn.xlbweb.gateway.constant.GwCommonConstants;
import cn.xlbweb.gateway.constant.GwOrderedConstants;
import cn.xlbweb.gateway.exception.GwException;
import cn.xlbweb.gateway.exception.GwExceptionEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author: bobi
 * @date: 2022/5/13 下午10:00
 * @description:
 */
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

    private final Logger logger = LoggerFactory.getLogger(AuthGlobalFilter.class);

    @Value("${cn.xlbweb.check-subscriber:true}")
    private Boolean checkSubscriber;

    @Override
    public int getOrder() {
        return GwOrderedConstants.ORDERED_00;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        logger.info("AuthGlobalFilter...");
        // 校验订阅者
        checkSubscriber(exchange);
        return chain.filter(exchange);
    }

    private void checkSubscriber(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String authorization = exchange.getRequest().getHeaders().getFirst(GwCommonConstants.HEADER_AUTHORIZATION);
        if (checkSubscriber) {
            if (StringUtils.isBlank(authorization)) {
                logger.error(GwExceptionEnum.AUTHORIZATION_NOT_NULL.getMessage());
                // throw new GatewayException("9999", "请求头authorization不能为空");
                throw new GwException(GwExceptionEnum.AUTHORIZATION_NOT_NULL);
            }
        }
    }
}
