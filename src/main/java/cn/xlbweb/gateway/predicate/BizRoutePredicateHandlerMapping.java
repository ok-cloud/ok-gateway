package cn.xlbweb.gateway.predicate;

import org.springframework.cloud.gateway.config.GlobalCorsProperties;
import org.springframework.cloud.gateway.handler.FilteringWebHandler;
import org.springframework.cloud.gateway.handler.RoutePredicateHandlerMapping;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

/**
 * @author: bobi
 * @date: 2022/5/24 上午11:35
 * @description:
 */
@Component
public class BizRoutePredicateHandlerMapping extends RoutePredicateHandlerMapping {

    public BizRoutePredicateHandlerMapping(FilteringWebHandler webHandler, RouteLocator routeLocator, GlobalCorsProperties globalCorsProperties, Environment environment) {
        super(webHandler, routeLocator, globalCorsProperties, environment);
    }

    @Override
    protected void validateRoute(Route route, ServerWebExchange exchange) {
        // System.out.println("requestHeaders=" + exchange.getRequest().getHeaders());
        // System.out.println("requestBody=" + exchange.getRequest().getBody());

        // System.out.println("responseHeaders=" + exchange.getResponse().getHeaders());
    }
}
