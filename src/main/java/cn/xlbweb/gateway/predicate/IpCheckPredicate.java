package cn.xlbweb.gateway.predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.cloud.gateway.handler.predicate.GatewayPredicate;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author: bobi
 * @date: 2022/5/13 下午4:38
 * @description:
 */
@Component
public class IpCheckPredicate extends AbstractRoutePredicateFactory<IpCheckPredicate.Config> {

    private final Logger logger = LoggerFactory.getLogger(IpCheckPredicate.class);

    public IpCheckPredicate() {
        super(IpCheckPredicate.Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("ip");
    }

    @Override
    public Predicate<ServerWebExchange> apply(IpCheckPredicate.Config config) {
        return (GatewayPredicate) serverWebExchange -> {
            if (config.getIp().equals("127.0.0.1")) {
                logger.info("match ip...");
                return true;
            }
            logger.error("not match ip...");
            return false;
        };
    }

    public static class Config {
        private String ip;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }
    }
}
