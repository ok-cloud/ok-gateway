package cn.xlbweb.gateway.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * @author: bobi
 * @date: 2022/5/16 下午9:29
 * @description:
 */
public class ApiRest implements Serializable {

    private static final long serialVersionUID = -568408263538307036L;

    private HttpStatus httpStatus;
    private String traceId = MDC.get("traceId");
    private String path;
    private String code;
    private String message;

    public ApiRest(HttpStatus httpStatus, String path, String code, String message) {
        this.httpStatus = httpStatus;
        this.path = path;
        this.code = code;
        this.message = message;
    }

    @JsonIgnore
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getPath() {
        return path;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ApiRest response(HttpStatus httpStatus, String path, String code, String message) {
        return new ApiRest(httpStatus, path, code, message);
    }
}
