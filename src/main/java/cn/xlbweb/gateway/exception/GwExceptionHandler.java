package cn.xlbweb.gateway.exception;

import cn.xlbweb.gateway.common.ApiRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.HttpMessageReader;
import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.view.ViewResolver;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

/**
 * @author: bobi
 * @date: 2022/5/17 上午10:41
 * @description:
 */
public class GwExceptionHandler implements ErrorWebExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GwExceptionHandler.class);

    private List<HttpMessageReader<?>> messageReaders = Collections.emptyList();

    private List<HttpMessageWriter<?>> messageWriters = Collections.emptyList();

    private List<ViewResolver> viewResolvers = Collections.emptyList();

    private ThreadLocal<ApiRest> exceptionHandlerResult = new ThreadLocal<>();

    public void setMessageReaders(List<HttpMessageReader<?>> messageReaders) {
        Assert.notNull(messageReaders, "'messageReaders' must not be null");
        this.messageReaders = messageReaders;
    }

    public void setViewResolvers(List<ViewResolver> viewResolvers) {
        this.viewResolvers = viewResolvers;
    }

    public void setMessageWriters(List<HttpMessageWriter<?>> messageWriters) {
        Assert.notNull(messageWriters, "'messageWriters' must not be null");
        this.messageWriters = messageWriters;
    }

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        ServerHttpRequest request = exchange.getRequest();
        ApiRest apiRest = null;
        if (ex instanceof GwException) {
            GwException gatewayException = (GwException) ex;
            apiRest = ApiRest.response(HttpStatus.INTERNAL_SERVER_ERROR, request.getPath().value(), gatewayException.getCode(), gatewayException.getMessage());
        } else if (ex instanceof ResponseStatusException) {
            ResponseStatusException responseStatusException = (ResponseStatusException) ex;
            if (responseStatusException.getStatus().value() == HttpStatus.NOT_FOUND.value()) {
                apiRest = ApiRest.response(HttpStatus.NOT_FOUND, request.getPath().value(), "999990", "网关路由不到目标服务上");
            } else {
                apiRest = ApiRest.response(responseStatusException.getStatus(), request.getPath().value(), "999991", responseStatusException.getReason());
            }
        } else {
            apiRest = ApiRest.response(HttpStatus.INTERNAL_SERVER_ERROR, request.getPath().value(), "999999", "未知异常，请联系管理员");
        }

        logger.error("全局异常处理，路径{}", request.getPath(), ex);

        if (exchange.getResponse().isCommitted()) {
            return Mono.error(ex);
        }

        exceptionHandlerResult.set(apiRest);

        ServerRequest newRequest = ServerRequest.create(exchange, this.messageReaders);
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse).route(newRequest).switchIfEmpty(Mono.error(ex)).flatMap((handler) -> handler.handle(newRequest)).flatMap((response) -> write(exchange, response));
    }

    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        ApiRest apiRest = exceptionHandlerResult.get();
        return ServerResponse.status(apiRest.getHttpStatus()).contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(apiRest));
    }

    private Mono<? extends Void> write(ServerWebExchange exchange, ServerResponse response) {
        exchange.getResponse().getHeaders().setContentType(response.headers().getContentType());
        return response.writeTo(exchange, new ResponseContext());
    }

    private class ResponseContext implements ServerResponse.Context {

        @Override
        public List<HttpMessageWriter<?>> messageWriters() {
            return GwExceptionHandler.this.messageWriters;
        }

        @Override
        public List<ViewResolver> viewResolvers() {
            return GwExceptionHandler.this.viewResolvers;
        }
    }
}
