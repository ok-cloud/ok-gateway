package cn.xlbweb.gateway.exception;

/**
 * @author: bobi
 * @date: 2022/5/16 下午9:00
 * @description:
 */
public class GwException extends RuntimeException {

    private String code;
    private String message;

    public GwException(GwExceptionEnum gatewayExceptionEnum) {
        this.code = gatewayExceptionEnum.getCode();
        this.message = gatewayExceptionEnum.getMessage();
    }

    public GwException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
