package cn.xlbweb.gateway.exception;

/**
 * @author: bobi
 * @date: 2022/7/25 下午10:41
 * @description:
 */
public enum GwExceptionEnum {

    AUTHORIZATION_NOT_NULL("9999", "请求头authorization不能为空!!!");

    private String code;
    private String message;

    GwExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
